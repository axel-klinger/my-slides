---
author: Axel Klinger
title: GitLab Workshop I - OERcamp Lübeck 2019
date: 13./14. Juni 2019
revealjs-url: 'reveal.js'
theme: isovera
css:
  - 'https://fonts.googleapis.com/css?family=Roboto+Slab:700'
---

## Über mich

- CTO der TIB
- Open Enthusiast

![Test](reveal.js/images/frage.jpg){ style="border:0; background:none; height:50px;" }\

# Ein Oberthema

## Ein Unterthema

- Each slide begins with an `<h2>` element (`"## "` in markdown).
- Use an `<h1>` element (`"# "` in markdown) to group slides.

## Ziel

> Open Education.
