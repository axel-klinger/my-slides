# .SUFFIXES: .html .md
SRCS := $(wildcard markdown/*.md)
HTML := $(SRCS:markdown/%.md=%.html)
vpath %.md markdown
vpath %.html html

%.html: %.md
	pandoc --standalone -t revealjs -o html/$@ $<

default: oer-ws-1.html

build: $(HTML) index.md
	pandoc --standalone -o html/index.html index.md
	cp -R images reveal.js html
